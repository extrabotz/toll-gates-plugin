## Arcturus Toll Gates

Convert one way gates on Arcturus Emulator to chargeable toll gates

[Download tollgates.jar](https://bitbucket.org/shniebs/toll-gates-plugin/wiki/Home)

---

## How to use

[Check out the Wiki](https://bitbucket.org/shniebs/toll-gates-plugin/wiki/Home)