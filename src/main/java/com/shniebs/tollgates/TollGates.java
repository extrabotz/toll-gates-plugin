/*
 * Name: Brandon McCulloch
 *
 * Date: 2019-01-06
 *
 * Arcturus Toll Gates Plugin
 *
 * Convert one way gates on Arcturus Emulator
 * to chargeable toll gates
 */
package com.shniebs.tollgates;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.CommandHandler;
import com.eu.habbo.habbohotel.items.ItemInteraction;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadItemsManagerEvent;
import com.shniebs.tollgates.commands.PayTollCommand;
import com.shniebs.tollgates.commands.SetTollCommand;
import com.shniebs.tollgates.database.DatabaseManager;
import com.shniebs.tollgates.items.TollGateInteraction;

public class TollGates extends HabboPlugin implements EventListener {

    public TollGates()
    {
        Emulator.getPluginManager().registerEvents(this, this);
        CommandHandler.addCommand(SetTollCommand.class);
        CommandHandler.addCommand(PayTollCommand.class);
    }

    public void onEnable() {
        Emulator.getLogging().logStart("[Shniebs] Toll Gates has Started");
        DatabaseManager.createTable();
    }

    public void onDisable() {
        Emulator.getLogging().logStart("[Shniebs] Toll Gates has Stopped");
    }

    public boolean hasPermission(Habbo habbo, String s) {
        if(s.equals("toll_set") || s.equals("toll_pay")) {
            return true;
        }
        return false;
    }

    @EventHandler
    public static void onItemsLoading(EmulatorLoadItemsManagerEvent event) {
        Emulator.getGameEnvironment().getItemManager().addItemInteraction(new ItemInteraction("tollgate", TollGateInteraction.class));
    }
}
