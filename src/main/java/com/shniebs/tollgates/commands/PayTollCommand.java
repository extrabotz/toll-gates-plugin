package com.shniebs.tollgates.commands;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.rooms.RoomTile;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.habbohotel.users.HabboInfo;
import com.eu.habbo.habbohotel.users.HabboItem;
import com.eu.habbo.habbohotel.users.HabboManager;
import com.shniebs.tollgates.database.DatabaseManager;
import com.shniebs.tollgates.items.GateChecker;
import com.shniebs.tollgates.items.TollGateInteraction;

public class PayTollCommand extends Command {

    public PayTollCommand() {
        super("toll_pay", new String[]{"paytoll"});
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) throws Exception {

        // Room, user tile, and item tile variables
        Room room = gameClient.getHabbo().getHabboInfo().getCurrentRoom();
        RoomTile habboTile = gameClient.getHabbo().getRoomUnit().getCurrentLocation();
        RoomTile gateTile = room.getLayout().getTileInFront(habboTile, gameClient.getHabbo().getRoomUnit().getBodyRotation().getValue());

        // Get Body Rotation to Patch Diagonal Bug
        int rotation = gameClient.getHabbo().getRoomUnit().getBodyRotation().getValue();

        // Gate Variables
        int gateCount = 0;
        HabboItem tollGate = null;

        // If the tile in front of the user is null
        if(gateTile == null) {
            gameClient.getHabbo().whisper("You are not positioned in front of a toll gate!");
            return true;
        } else if(rotation == 1 || rotation == 3 || rotation == 5 || rotation == 7) {
            gameClient.getHabbo().whisper("You can not pay for a toll diagonally!");
            return true;
        }

        for(HabboItem furni : room.getFloorItems()) {

            // Check if this item is on the gate tile
            if(gateTile == room.getLayout().getTile(furni.getX(), furni.getY())) {

                // Creates a gateChecker
                GateChecker gateChecker = new GateChecker();

                // Returns true if the item is a one_way_gate
                if(gateChecker.gateCheck(furni)) {

                    // Save the furni to the tollGate variable and increment the counter
                    tollGate = furni;
                    gateCount++;
                }

            }
        }

        // If no toll gate has been found then alert the user
        if(tollGate == null) {
            gameClient.getHabbo().whisper("You are not positioned in front of a toll gate!");
            return true;
        } else if (!DatabaseManager.hasToll(tollGate.getId())) {
            gameClient.getHabbo().whisper("This gate doesn't have a toll!");
            return true;
        }

        // More Toll Gate Variables
        TollGateInteraction oneWayGate = (TollGateInteraction) tollGate;
        String tollType = DatabaseManager.getTollType(tollGate.getId());
        int tollCost = DatabaseManager.getTollCost(tollGate.getId());
        int tollOwner = DatabaseManager.getTollOwner(tollGate.getId());



        // The owner of the toll gate
        HabboInfo owner = Emulator.getGameEnvironment().getHabboManager().getOfflineHabboInfo(tollOwner);
        Habbo ownerOnline = Emulator.getGameEnvironment().getHabboManager().getHabbo(tollOwner);

        if(gateCount == 1) {

            // check if the user can pay
            if(tollType.equalsIgnoreCase("credits")) {

                // insufficient credits
                if(gameClient.getHabbo().getHabboInfo().getCredits() >= tollCost) {
                    gameClient.getHabbo().giveCredits(-tollCost);
                    if(ownerOnline != null) {
                        ownerOnline.whisper("You received " + tollCost +
                                " " + tollType + " from " + gameClient.getHabbo().getHabboInfo().getUsername());
                        ownerOnline.giveCredits(tollCost);
                    } else {
                        owner.addCredits(tollCost);
                    }
                } else {
                    gameClient.getHabbo().whisper("You do not have enough credits!");
                    return true;
                }
            } else if(tollType.equalsIgnoreCase("duckets")) {

                // insufficient duckets
                if(gameClient.getHabbo().getHabboInfo().getPixels() >= tollCost) {
                    gameClient.getHabbo().givePixels(-tollCost);
                    if(ownerOnline != null) {
                        ownerOnline.whisper("You received " + tollCost +
                                " " + tollType + " from " + gameClient.getHabbo().getHabboInfo().getUsername());
                        ownerOnline.givePixels(tollCost);
                    } else {
                        owner.addPixels(tollCost);
                    }
                } else {
                    gameClient.getHabbo().whisper("You do not have enough duckets!");
                    return true;
                }
            } else if(tollType.equalsIgnoreCase("diamonds")) {

                // insufficient diamonds
                if(gameClient.getHabbo().getHabboInfo().getCurrencyAmount(5) >= tollCost) {
                    gameClient.getHabbo().givePoints(5, -tollCost);
                    if(ownerOnline != null) {
                        ownerOnline.whisper("You received " + tollCost +
                                " " + tollType + " from " + gameClient.getHabbo().getHabboInfo().getUsername());
                        ownerOnline.givePoints(5, tollCost);
                    } else {
                        owner.addCurrencyAmount(5, tollCost);
                    }
                } else {
                    gameClient.getHabbo().whisper("You do not have enough diamonds!");
                    return true;
                }
            }

            // If the user makes it here they have paid
            gameClient.getHabbo().whisper("You paid " + tollCost + " " +
                    tollType + " to " + HabboManager.getOfflineHabboInfo(tollOwner).getUsername());

            // Handles the onClick interaction
            Object[] objects = new Object[0];
            oneWayGate.onClick(gameClient, room, objects, true);

        } else if(gateCount > 1) {
            gameClient.getHabbo().whisper("More than 1 toll gate was found.");
        } else {
            gameClient.getHabbo().whisper("No toll gates were found.");
        }

        return true;
    }

}
