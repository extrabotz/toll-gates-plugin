package com.shniebs.tollgates.commands;

import com.shniebs.tollgates.database.DatabaseManager;
import com.shniebs.tollgates.items.GateChecker;
import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.habbohotel.users.HabboItem;
import gnu.trove.set.hash.THashSet;

public class SetTollCommand extends Command {

    public SetTollCommand() {
        super("toll_set", new String[]{"settoll"});
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) {

            // Gets user information and coordinates
            Habbo user = gameClient.getHabbo();
            short userX = user.getRoomUnit().getX();
            short userY = user.getRoomUnit().getY();

            // Check that there is at least 1 parameter
            if (strings.length < 3) {
                user.whisper("Improper usage! :settoll <type> <amount>");
                return true;
            }

            String tollType;
            String tollAmountString;
            int tollAmount;

            // Checks for proper command format
            if ((strings[1].equalsIgnoreCase("credits")
                    || strings[1].equalsIgnoreCase("diamonds")
                    || strings[1].equalsIgnoreCase("duckets")) && !strings[2].isEmpty()) {
                try {
                    tollType = strings[1];
                    tollAmountString = strings[2];
                    tollAmount = Integer.parseInt(tollAmountString);
                } catch (Exception e) {
                    user.whisper("Improper usage! :settoll <type> <amount>");
                    return true;
                }
            } else {
                user.whisper("Improper usage! :settoll <type> <amount>");
                return true;
            }

            if (tollAmount < 0) {
                gameClient.getHabbo().whisper("You cannot set a negative toll!");
                return true;
            }

            // Gets the current room
            Room room = user.getHabboInfo().getCurrentRoom();

            // All furniture in the room
            THashSet<HabboItem> furniture = room.getFloorItems();

            // If gateCount is 0 by the end of the loop then no gates are found
            int gateCount = 0;

            // Creates a gateChecker
            GateChecker gateChecker = new GateChecker();

            // Loop puts each furniture into a HabboItem
            for (HabboItem x : furniture) {

                // Checks for a one way gate in the room
                if (gateChecker.gateCheck(x)) {

                    // Checks if the user is in front of the toll gate
                    if ((x.getX() == userX && (userY == x.getY() + 1 || userY == x.getY() - 1))
                            || (x.getY() == userY && (userX == x.getX() + 1 || userX == x.getX() - 1))) {

                        // Make sure the gate belongs to the user
                        if(user.getHabboInfo().getId() == x.getUserId()) {

                            // add 1 to the gate count
                            ++gateCount;

                            if(tollAmount == 0) {
                                DatabaseManager.removeToll(x.getId());
                            } else {
                                // adds toll to the database
                                DatabaseManager.addToll(x.getId(), x.getUserId(), tollType, tollAmount);
                            }
                        }
                    }
                } // end of gate check
            } // end of for each loop

            if (gateCount < 1) {
                user.whisper("You are not in front of your own gate.");
            } else if (gateCount == 1) {
                if(tollAmount == 0) {
                    user.whisper("Toll removed from " + gateCount + " gate.");
                } else {
                    user.whisper("Toll added to " + gateCount + " gate.");
                }
            } else {
                if(tollAmount == 0) {
                    user.whisper("Toll removed from " + gateCount + " gates.");
                } else {
                    user.whisper("Toll added to " + gateCount + " gates.");
                }
            }

        return true;

    }
}