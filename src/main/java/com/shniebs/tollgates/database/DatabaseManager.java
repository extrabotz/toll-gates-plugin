package com.shniebs.tollgates.database;

import com.eu.habbo.Emulator;
import java.sql.*;

public class DatabaseManager {

    private static Connection getConnection() {

        Connection conn = null;

        try {
            conn = Emulator.getDatabase().getDataSource().getConnection();
            return conn;
        } catch (SQLException e) {
            Emulator.getLogging().logDebugLine(e);
        }

        return conn;

    }

    public static void createTable() {
        Connection conn = getConnection();
        PreparedStatement create;
        try {
            create = conn.prepareStatement("CREATE TABLE IF NOT EXISTS items_tollgates(id INT NOT NULL AUTO_INCREMENT," +
                    "item_id INT NOT NULL," +
                    "owner_id INT NOT NULL," +
                    "type VARCHAR(255) NOT NULL," +
                    "amount INT NOT NULL,PRIMARY KEY (`id`))");
            create.executeUpdate();
            conn.close();
        } catch (SQLException e) {
           e.printStackTrace();
        }

    }

    public static void addToll(int item_id, int owner_id, String type, int amount) {
        Connection conn = getConnection();
        PreparedStatement insert;
        try {
            if(hasToll(item_id)) {
                insert = conn.prepareStatement("UPDATE items_tollgates SET " +
                        "owner_id = ?, type = ?, amount = ? WHERE item_id = ?");
                insert.setInt(1, owner_id);
                insert.setString(2, type);
                insert.setInt(3, amount);
                insert.setInt(4, item_id);
            } else {
                insert = conn.prepareStatement("INSERT INTO items_tollgates(item_id, owner_id, type, amount) " +
                        "VALUES (?, ?, ?, ?)");
                insert.setInt(1, item_id);
                insert.setInt(2, owner_id);
                insert.setString(3, type);
                insert.setInt(4, amount);
            }
            insert.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeToll(int id) {
        Connection conn = getConnection();
        PreparedStatement remove;
        try {
            remove = conn.prepareStatement("DELETE FROM items_tollgates WHERE item_id = ?");
            remove.setInt(1, id);
            remove.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getTollOwner(int id) {
        Connection conn = getConnection();
        PreparedStatement owner;
        int ownerId = 0;
        try {
            owner = conn.prepareStatement("SELECT owner_id FROM items_tollgates WHERE item_id = ?");
            owner.setInt(1, id);
            owner.executeQuery();
            ResultSet set = owner.executeQuery();
            set.next();
            ownerId = set.getInt("owner_id");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ownerId;
    }

    public static int getTollCost(int id) {
        Connection conn = getConnection();
        PreparedStatement check;
        int tollCost = 0;
        try {
            check = conn.prepareStatement("SELECT amount FROM items_tollgates WHERE item_id = ?");
            check.setInt(1, id);
            ResultSet set = check.executeQuery();
            set.next();
            tollCost = set.getInt("amount");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tollCost;
    }

    public static String getTollType(int id) {
        Connection conn = getConnection();
        PreparedStatement check;
        String tollType = null;
        try {
            check = conn.prepareStatement("SELECT type FROM items_tollgates WHERE item_id = ?");
            check.setInt(1, id);
            ResultSet set = check.executeQuery();
            set.next();
            tollType = set.getString("type");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tollType;
    }

    public static boolean hasToll(int id) {
        Connection conn = getConnection();
        PreparedStatement count;
        int countTotal = 0;
        try {
            count = conn.prepareStatement("SELECT COUNT(id) AS total FROM items_tollgates WHERE item_id = ?");
            count.setInt(1, id);
            ResultSet set = count.executeQuery();
            set.next();
            countTotal = set.getInt("total");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(countTotal > 0) {
            return true;
        }
        return false;
    }

}
