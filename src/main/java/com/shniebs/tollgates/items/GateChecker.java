package com.shniebs.tollgates.items;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.items.Item;
import com.eu.habbo.habbohotel.items.ItemInteraction;
import com.eu.habbo.habbohotel.users.HabboItem;
import com.shniebs.tollgates.TollGates;

public class GateChecker {

    Item aquaGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*1");
    Item blackGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*2");
    Item whiteGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*3");
    Item beigeGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*4");
    Item pinkGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*5");
    Item blueGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*6");
    Item greenGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*7");
    Item yellowGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*8");
    Item redGate = Emulator.getGameEnvironment().getItemManager().getItem("one_way_door*9");

    public boolean gateCheck(HabboItem x) {

        if (x.getBaseItem() == aquaGate) return true;
        if (x.getBaseItem() == blackGate) return true;
        if (x.getBaseItem() == whiteGate) return true;
        if (x.getBaseItem() == beigeGate) return true;
        if (x.getBaseItem() == pinkGate) return true;
        if (x.getBaseItem() == blueGate) return true;
        if (x.getBaseItem() == greenGate) return true;
        if (x.getBaseItem() == yellowGate) return true;
        if (x.getBaseItem() == redGate) return true;

        return false;

    }

}
