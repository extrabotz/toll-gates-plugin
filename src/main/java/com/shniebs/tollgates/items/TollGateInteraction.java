package com.shniebs.tollgates.items;

import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.habbohotel.items.Item;
import com.eu.habbo.habbohotel.items.interactions.InteractionOneWayGate;
import com.eu.habbo.habbohotel.rooms.Room;
import com.shniebs.tollgates.database.DatabaseManager;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TollGateInteraction extends InteractionOneWayGate {

    public TollGateInteraction(ResultSet set, Item baseItem) throws SQLException {
        super(set, baseItem);
    }

    public TollGateInteraction (int id, int userId, Item item, String extradata, int limitedStack, int limitedSells) {
        super(id, userId, item, extradata, limitedStack, limitedSells);
    }

    public void onClick(GameClient client, Room room, Object[] objects) throws Exception {

        if(DatabaseManager.hasToll(this.getId())) {
            String tollType = DatabaseManager.getTollType(this.getId());
            int tollCost = DatabaseManager.getTollCost(this.getId());

            // inform the user of the toll
            client.getHabbo().whisper("This gate has a " + tollType + " toll of " + tollCost);
            client.getHabbo().whisper("Use :paytoll and face the gate");

        } else {
            super.onClick(client, room, objects);
        }
    }

    public void onClick(GameClient client, Room room, Object[] objects, Boolean hasPaid) throws Exception {
        if(hasPaid) {
            super.onClick(client, room, objects);
        }
    }

}